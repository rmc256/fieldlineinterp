#!/usr/bin python

'''
Example code to interpolate particle position along a field line
This assumes we are given a particle's positions at two consecutive time slices,
and we want to interpolate for several positions in between
NOTE: This code simply assumes the particle travels along a field line (often a good
assumption for small time intervals). It does not (currently) handle several possibly
important cases:
1. Significant perpendicular motion between time slices
2. Trapped particle motion
3. Reflection from material walls
4. Particle traveling more than one toroidal turn
Some of these can be alleviated with more work, or simply having higher time sampling

Algorithm
Solve the system of coupled first order ODEs defining field line:
d\vec{L} x \vec{B} = 0 =>	[dR/dphi,	= [R*BR/Bphi,
 			  	 			 dZ/dphi,	   R*BZ/Bphi,
 			  	 			 dphi/dphi]	   1]

i.e. d\vec{L}/dphi = \vec{f}(\vec{L})

Here we use the SciPy ODE solver odeint(), but many other solvers
could be used.
'''

import h5py
import numpy as np
from scipy.integrate import odeint
from scipy.interpolate import LinearNDInterpolator
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def interpolate_fieldLineFollow(Lstart,phiEnd,Binterp):

	#define RHS of ODE system of equations, dy/dt = f(y,t)
	def f(L,phi,Binterp):
		R=L[0]
		Z=L[1]
		B=Binterp(R,Z)
		BR=B[0]
		BZ=B[1]
		Bphi=B[2]
		#model equations
		f0 = R*BR/Bphi
		f1 = R*BZ/Bphi
		#f2 = 1.
		return [f0,f1]#,f2]

	#create an array of phi coordinates for which the particle position
	#will be calculated, in between the initial and end phi poisitions
	Npts = 100
	phi = np.linspace(Lstart[2],phiEnd,Npts)
	dphi = phi[1]-phi[0]

	soln = odeint(f,Lstart[0:2],phi,args=(Binterp,))
	Lout = np.hstack((soln,phi[:,np.newaxis]))
	return Lout

#example code of how to use
fileDir = '/project/projectdirs/m499/jlang/particle_pinch/'
fileBfield = fileDir + 'xgc.bfield.h5'
fb = h5py.File(fileBfield,'r')
Bgrid = fb['node_data[0]/values'][:]

fileMesh = fileDir + 'xgc.mesh.h5'
fm = h5py.File(fileMesh,'r')
RZ = fm['coordinates/values'][:]

#setup Bfield interpolator (could use higher order interpolation scheme)
Binterp = LinearNDInterpolator(RZ, Bgrid, fill_value = np.inf)

Lstart = np.array([2.2,0.,0.]) #starting (R,Z,phi) position
phiEnd = 8*np.pi               #ending phi position
Lout = interpolate_fieldLineFollow(Lstart,phiEnd,Binterp) #interpolated particle trajectory along field line

#plot in the R,Z plane
plt.figure(1)
plt.plot(Lout[:,0],Lout[:,1],'-x')

#plot in 3D geometry
fig = plt.figure(2)
ax = fig.add_subplot(111,projection='3d')
X=Lout[:,0]*np.cos(-Lout[:,2])
Y=Lout[:,0]*np.sin(-Lout[:,2])
Z=Lout[:,1]
ax.plot(X,Y,Z)

plt.show()
