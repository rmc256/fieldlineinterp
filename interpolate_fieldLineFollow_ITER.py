#!/usr/bin python

'''
Example code to interpolate particle position along a field line
This assumes we are given a particle's positions at two consecutive time slices,
and we want to interpolate for several positions in between. It attempts to unwrap the end
toroidal angle by using the particles parallel velocity.
NOTE: This code simply assumes the particle travels along a field line (often a good
assumption for small time intervals). It does not (currently) handle several possibly
important cases:
1. Significant perpendicular motion between time slices
2. Trapped particle motion
3. Reflection from material walls
Some of these can be alleviated with more work, or simply having higher time sampling

Algorithm
Solve the system of coupled first order ODEs defining field line:
d\vec{L} x \vec{B} = 0 =>	[dR/dphi,	= [R*BR/Bphi,
 			  	 			 dZ/dphi,	   R*BZ/Bphi,
 			  	 			 dphi/dphi]	   1]

i.e. d\vec{L}/dphi = \vec{f}(\vec{L})

Here we use the SciPy ODE solver odeint(), but many other solvers
could be used.
'''
import h5py
import numpy as np
from scipy.integrate import odeint
from scipy.interpolate import LinearNDInterpolator
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def interpolate_fieldLineFollow(Lstart,phiEnd,Binterp):

	#define RHS of ODE system of equations, dy/dt = f(y,t)
	def f(L,phi,Binterp):
		R=L[0]
		Z=L[1]

		B=Binterp(R,Z)
		BR=B[0]
		BZ=B[1]
		Bphi=B[2]

		#model equation
		f0 = R*BR/Bphi
		f1 = R*BZ/Bphi
		#f2 = 1.
		return [f0,f1]#,f2]


	#create an array of phi coordinates for which the particle position
	#will be calculated, in between the initial and end phi poisitions
	Npts = 400
	phi = np.linspace(Lstart[2],phiEnd,Npts)

	soln = odeint(f,Lstart[0:2],phi,args=(Binterp,))
	Lout = np.hstack((soln,phi[:,np.newaxis]))
	return Lout


# ###get ITER simulation data
fileDir = '/scratch2/scratchdirs/sku/ti240_ITER_particle/'
fileMesh = fileDir + 'xgc.mesh.h5'
fm = h5py.File(fileMesh,'r')
RZ = fm['coordinates/values'][:]

fileDirB = '/global/project/projectdirs/m499/rchurchi/interp_field/'
fileBfield = fileDirB + 'xgc.bfield.h5'
fb = h5py.File(fileBfield,'r')
Bgrid = fb['node_data[0]/values'][:]
#setup Bfield interpolator (could use higher order interpolation scheme)
Binterp = LinearNDInterpolator(RZ, Bgrid, fill_value = np.inf)

#get particle information
filePart1 = fileDir + 'xgc.particle.00002.h5'
filePart2 = fileDir + 'xgc.particle.00004.h5'
# gid = 3856000001#good, psin=0.96
# gid = 95046500001#bad, slow, LFS only trajectory
# gid = 18926000001 #tgood, rapped particle, can't handle
# gid = 65170000001 #bad, psin=0.95, misses a complete turn
# gid = 2332500001 #bad, slow, LFS only trajectory
fp1 = h5py.File(filePart1,'r')
egid1 = fp1['egid'][:]
gid = egid1[np.random.randint(0,high=egid1.size)]
ephase1 = np.squeeze(fp1['ephase'][egid1 == gid,0:4]) #this gives [R,Z,phi,rho_parallel]
fp2 = h5py.File(filePart2,'r')
egid2 = fp2['egid'][:]
ephase2 = np.squeeze(fp2['ephase'][egid2 == gid,0:4]) #this gives [R,Z,phi,rho_parallel]

if np.sign(ephase1[3]) != np.sign(ephase2[3]):
	raise ValueError('rho_parallel changes sign; not able to handle trapped particle motion')

########NOTE: These are specific inputs from the ITER simulation, mosltly ##############
########      found in units.m, but some from Seung-Hoe				 ###############
mass_ratio = 1e3 #mp/me
me = 1.667e-27/mass_ratio #electron mass used in simulation
dt = 2*8.6632492620251972e-7 #time between particle file outputs
R0 = 6.362604263513000 #major radius of tokamak
B0 = -5.300000
###############################################################################
q = -1.609e-19 #electron charge

Lstart = ephase1[0:3] #starting (R,Z,phi) position
Lend = ephase2[0:3]
phiStart = Lstart[2]
phiEnd = Lend[2]        #ending phi position

##ESTIMATE phiEnd
#find the correct phiEnd by estimating how far the particle will travel based
#on its parallel velocity
rhoParallelAvg = (ephase1[3]+ephase2[3])/2.
B = np.sign(B0)*np.sqrt(np.sum(Binterp(Lstart[0],Lstart[1])**2.))
vParallelAvg = rhoParallelAvg*q/me*B0
Lest = np.abs(vParallelAvg*dt)
phiChangeEst = Lest/R0

#this approximation (NphiApprox) is from solving the following two equations:
#	phi2un = phi2 + sgn(rho_parallel)*N*2*pi
#	phi2un ~ phi1 + sgn(rho_parallel)*phiEst
#where phi1 and phi2 are the starting and ending toroidal angles, phi2un is the 
#unwrapped toroidal angle, N is the number of toroidal turns, phiEst is the estimated
#toroidal angle travelled based on the parallel velocity.
NphiApprox = (phiChangeEst + np.sign(rhoParallelAvg)*(phiStart-phiEnd))/(2.*np.pi)
NphiFloor,NphiRem = divmod(NphiApprox,1)
Nphi = np.round(NphiApprox)
phiEnd = Lend[2] + np.sign(rhoParallelAvg)*Nphi*2.*np.pi

Lout = interpolate_fieldLineFollow(Lstart,phiEnd,Binterp) #interpolated particle trajectory along field line
err1=np.sqrt(np.sum((Lout[-1,0:2]-Lend[0:2])**2.))

if err1 > 1e-2: #if further than 1cm from the end point, try other Nphi
	print 'Inaccurate Nphi estimate: attempting upper and lower'
	if NphiRem < 0.5:
		Nphi2 = np.ceil(NphiApprox)
	else:
		Nphi2 = np.floor(NphiApprox) 
	phiEnd2 = Lend[2] + np.sign(rhoParallelAvg)*Nphi2*2.*np.pi
	Lout2 = interpolate_fieldLineFollow(Lstart,phiEnd2,Binterp) #interpolated particle trajectory along field line

	err2=np.sqrt(np.sum((Lout2[-1,0:2]-Lend[0:2])**2.))
	if err2 < err1:
		Lout = Lout2
		Nphi = Nphi2
		phiEnd = phiEnd2

print Lout
print "end point should be " + str(Lend)

print ''
print 'gid ',gid
print 'rho_parallel 1',ephase1[3]
print 'rho_parallel 2',ephase2[3]
print 'NphiApprox ',NphiApprox
print 'Nphi ',Nphi
print 'phiChangeEst ',phiChangeEst
print 'phiStart ',phiStart
print 'phiEnd Orig',Lend[2]
print 'phiEnd in',phiEnd

fig1 = plt.figure(1)
plt.suptitle('Calculated end point (blue x) should overlay particle end point (red dot)')
plt.cla()
plt.plot(Lstart[0],Lstart[1],'go')
plt.plot(Lend[0],Lend[1],'ro')
plt.plot(Lout[:,0],Lout[:,1],'r')
plt.plot(Lout[-1,0],Lout[-1,1],'bx')
plt.show()
